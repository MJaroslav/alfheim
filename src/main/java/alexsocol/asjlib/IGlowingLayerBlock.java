package alexsocol.asjlib;

import net.minecraft.util.IIcon;

public interface IGlowingLayerBlock {
	
	public IIcon getGlowIcon(int side, int meta);
}